
package com.michel3951.cheststocks.Support.Database;


import com.michel3951.cheststocks.Support.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Sql {

    private static String path = "jdbc:sqlite:./plugins/ChestStocks/transactions.db";

    public static void createDatabase() {
        try (Connection conn = DriverManager.getConnection(path)) {
            if (conn != null) {
                conn.close();
                createTables();
            }
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

    }

    private static void createTables() {
        Connection conn = getConnection();

        if (conn == null) {
            return;
        }

        try {
            String query = "CREATE TABLE IF NOT EXISTS `transactions` (type TEXT, item TEXT, price DOUBLE, amount INT, timestamp TEXT); CREATE INDEX IF NOT EXISTS `item_idx` ON `transactions` (item);";
            Statement stmt = conn.createStatement();

            stmt.execute(query);
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }
    }

    public static Connection getConnection() {
        Connection conn = null;

        try {
            // create a connection to the database
            conn = DriverManager.getConnection(path);

            return conn;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        return null;
    }
}
