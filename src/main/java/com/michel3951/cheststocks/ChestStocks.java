package com.michel3951.cheststocks;

import com.michel3951.cheststocks.Events.OnChestShopTransaction;
import com.michel3951.cheststocks.Http.JettyServer;
import com.michel3951.cheststocks.Support.Chat;
import com.michel3951.cheststocks.Support.Database.Sql;
import com.michel3951.cheststocks.Support.Logger;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import net.milkbowl.vault.economy.Economy;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;

public final class ChestStocks extends JavaPlugin {

    private static Economy econ;
    private Connection conn;
    private JettyServer http;

    @Override
    public void onEnable() {
        Logger.info(Chat.colored("&aChestStocks has been enabled"));

        File f = new File(this.getDataFolder() + "/");
        if (!f.exists()) f.mkdir();

        initializeEconomy();
        Sql.createDatabase();
        conn = Sql.getConnection();

        try {
            http = new JettyServer();
            http.start(conn);
        } catch (Exception e) {
            e.printStackTrace();
        }


        getServer().getPluginManager().registerEvents(new OnChestShopTransaction(conn), this);
    }

    @Override
    public void onDisable() {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.error(e.getMessage());
            }
        }
        Logger.info(Chat.colored("&cChestStocks has been disabled"));
    }

    private boolean initializeEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }

        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }

        econ = rsp.getProvider();
        return econ != null;
    }


    public static Economy getEconomy() {
        return econ;
    }
}