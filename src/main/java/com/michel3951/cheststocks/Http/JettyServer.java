package com.michel3951.cheststocks.Http;

import com.michel3951.cheststocks.Http.Servlets.ItemServlet;
import com.michel3951.cheststocks.Http.Servlets.ListServlet;
import jakarta.servlet.DispatcherType;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.FilterMapping;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import java.sql.Connection;
import java.util.EnumSet;

public class JettyServer {

    private Server server;

    public void start(Connection conn) throws Exception {
        server = new Server();
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(8473);
        server.setConnectors(new Connector[]{connector});

        ServletContextHandler servletHandler = new ServletContextHandler();
        servletHandler.setAttribute("conn", conn);

        servletHandler.addServlet(ListServlet.class, "/list");
        servletHandler.addServlet(ItemServlet.class, "/item");

        FilterHolder cors = servletHandler.addFilter(CrossOriginFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));
        cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        cors.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,POST,HEAD");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin");

        server.setHandler(servletHandler);
        server.start();
    }
}