package com.michel3951.cheststocks.Http.Servlets;

import com.michel3951.cheststocks.Support.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ListServlet extends HttpServlet {

    protected Connection conn;

    @Override
    public void init() throws ServletException
    {
        conn = (Connection) getServletContext().getAttribute("conn");
    }

    protected void doGet(
            HttpServletRequest req,
            HttpServletResponse res)
            throws ServletException, IOException {

        res.setContentType("application/json");

        JSONArray re = distinctItems();

        res.setStatus(HttpServletResponse.SC_OK);
        res.getWriter().println(re.toJSONString());
    }

    private JSONArray distinctItems() {
        try {
            String query = "SELECT DISTINCT `item` FROM `transactions`";
            PreparedStatement stmt = conn.prepareStatement(query);
            ResultSet r = stmt.executeQuery();

            JSONArray results= new JSONArray();

            while (r.next()) {
                JSONObject obj = new JSONObject();

                obj.put("name", r.getString("item"));

                results.add(obj);
            }

            return results;
        } catch (SQLException ex) {
            Logger.error(ex.toString());
        }

        return new JSONArray();
    }
}