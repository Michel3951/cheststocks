package com.michel3951.cheststocks.Http.Servlets;

import com.michel3951.cheststocks.Support.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ItemServlet extends HttpServlet {

    protected Connection conn;

    @Override
    public void init() throws ServletException
    {
        conn = (Connection) getServletContext().getAttribute("conn");
    }

    protected void doGet(
            HttpServletRequest req,
            HttpServletResponse res)
            throws ServletException, IOException {

        res.setContentType("application/json");

        if (req.getParameter("item") == null) {
            res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            res.getWriter().println("{ \"message\": \"BAD_REQUEST\"}");
            return;
        }

        String name = req.getParameter("item");


        JSONArray re = getItem(name);

        res.setStatus(HttpServletResponse.SC_OK);
        res.getWriter().println(re.toJSONString());
    }

    private JSONArray getItem(String name) {
        try {
            String query = "SELECT * FROM `transactions` WHERE `item` = ?;";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, name);
            ResultSet r = stmt.executeQuery();

            JSONArray results= new JSONArray();

            while (r.next()) {
                JSONObject obj = new JSONObject();

                obj.put("side", r.getString("type"));
                obj.put("price", r.getDouble("price") / r.getInt("amount"));
                obj.put("time", r.getString("timestamp"));

                results.add(obj);
            }

            return results;
        } catch (SQLException ex) {
            Logger.error(ex.toString());
        }

        return new JSONArray();
    }
}